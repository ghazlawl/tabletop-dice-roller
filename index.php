<?php

include_once('./inc/funcs.inc.php');
include_once('./inc/processor.inc.php');

if (isset($existing_json)) {
  // Get the UI from the existing JSON.
  $ui = $existing_json->ui;
}

// Hold the profile name.
$name = '';

if (isset($_SESSION['name'])) {
  // Use the profile name that's stored in the session.
  $name = htmlentities($_SESSION['name']);
} elseif (isset($ui) && isset($ui->name) && !empty($ui->name)) {
  // Use the profile name that's stored in the cookie.
  $name = htmlentities($ui->name);
}

?><!DOCTYPE html>
<html>
<head>

  <title>Tabletop Dice Roller</title>

  <!-- Font Awesome -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>

  <!-- jQuery -->
  <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

  <!-- Cookies -->
  <script src="./resources/js/js.cookie.js"></script>

  <!-- Touch Punch -->
  <script src="./resources/js/jquery.ui.touch-punch.min.js"></script>

  <!-- Bootstrap -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <!-- Custom -->
  <link href="./resources/css/styles.css" rel="stylesheet"/>
  <script src="./resources/js/core.js"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="./resources/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="./resources/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="./resources/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="./resources/favicons/manifest.json">
  <link rel="mask-icon" href="./resources/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="./resources/favicons/favicon.ico">
  <meta name="msapplication-config" content="./resources/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>

</head>
<body class="<?php echo $_GET['theme']; ?> <?php echo isset($_SESSION['filename']) && !empty($_SESSION['filename']) ? 'existing' : 'new'; ?>">

<h1 class="title">Tabletop Dice Roller</h1>
<div class="title-version">v0.1</div>

<div id="top-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col col-xs-12 col-sm-2">
        <?php if (isset($_SESSION['message']) && !empty($_SESSION['message'])) : ?>
          <div class="post-message <?php echo $_SESSION['message']['class']; ?>">
            <?php echo $_SESSION['message']['message']; ?>
          </div><!-- /.post-message -->
        <?php endif; ?>
      </div><!-- /.col -->
      <div class="col col-xs-12 col-sm-10">
        <form name="login-form" id="login-form" action="./" method="post" autocomplete="off">
          <input type="hidden" name="presets" value=""/>
          <textarea name="log" value="" style="display: none;"></textarea>
          <textarea name="ui" value="" style="display: none;"></textarea>
          <input type="hidden" name="request" value="<?php echo $existing_json ? 'save' : 'load'; ?>"/>
          <div class="help-text">
            Enter a profile name and password to save or load a profile.
          </div><!-- /.help-text -->
          <div class="name-wrapper">
            <label for="name">Profile Name:</label>
            <input type="text" name="name" placeholder="Name" autocomplete="off" value="<?php echo $name; ?>"/>
          </div><!-- /.name-wrapper -->
          <div class="password-wrapper">
            <label for="password">Password:</label>
            <input type="password" name="password" placeholder="********" autocomplete="off" value="<?php echo htmlentities($_SESSION['password']); ?>"/>
          </div><!-- /.password-wrapper -->
          <div class="save-form-actions-wrapper">
            <?php if ($existing_json) : ?>
              <button name="submit" type="submit" form="login-form" class="save">Save</button>
              <button name="submit" type="submit" form="login-form" class="load">Reload</button>
              <button type="button" class="new">New Profile</button>
            <?php else : ?>
              <button name="submit" type="submit" form="login-form" class="save">Save</button>
              <button name="submit" type="submit" form="login-form" class="load">Load</button>
            <?php endif; ?>
          </div><!-- /.save-form-actions-wrapper -->
        </form><!-- /#login-form -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div><!-- /#top-wrapper -->

<div id="middle-wrapper">
  <div class="container-fluid">
    <div class="row">
      <?php if (isset($ui) && isset($ui->sortables)) : ?>
        <?php foreach ($ui->sortables as $sortable) : ?>
          <?php if ($sortable == 'form-sortable-col') : ?>
            <?php include_once('./inc/form.inc.php'); ?>
          <?php elseif ($sortable == 'presets-sortable-col') : ?>
            <?php include_once('./inc/presets.inc.php'); ?>
          <?php elseif ($sortable == 'log-sortable-col') : ?>
            <?php include_once('./inc/log.inc.php'); ?>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php else : ?>
        <?php include_once('./inc/form.inc.php'); ?>
        <?php include_once('./inc/presets.inc.php'); ?>
        <?php include_once('./inc/log.inc.php'); ?>
      <?php endif; ?>
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div><!-- /#middle-wrapper -->

<div id="dialog" title="Dialog Placeholder" style="display: none;">
  <p>This is placeholder text.</p>
</div>

<div class="device-xs visible-xs"></div>
<div class="device-sm visible-sm"></div>
<div class="device-md visible-md"></div>
<div class="device-lg visible-lg"></div>

<script src="https://use.typekit.net/vno0alt.js"></script>
<script>
  try {
    Typekit.load({async: true});
  } catch (e) {
  }
</script>

</body>
</html>

<?php unset($_SESSION['message']); ?>
