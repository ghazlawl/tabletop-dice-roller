/**
 * @file
 *
 * Contains all of the core functionality for the Tabletop Dice Roller.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */

/**
 * This value is populated via Ajax when the document ready event triggers.
 * When loaded, it contains placeholders that are replaced at runtime, is used
 * whenever presets are saved or loaded, and is shared with PHP.
 */
var PRESET_HTML_TEMPLATE = '';

// ============================== //
// Prototype Functions
// ============================== //

/**
 * Replaces all occurrences of a value within a string.
 *
 * @param search
 * @param replacement
 *
 * @returns {string}
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

/**
 * The JavaScript version of PHP's html_entity_encode().
 *
 * @todo Add the remaining html_entity_encode() equivalent actions.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
String.prototype.htmlEntityEncode = function () {
    var target = this;
    return target.replaceAll('"', '&quot;');
};

/**
 * The JavaScript version of PHP's html_entity_decode().
 *
 * @todo Add the remaining html_entity_decode() equivalent actions.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
String.prototype.htmlEntityDecode = function () {
    var target = this;
    return target.replaceAll('&quot;', '"');
};

// ============================== //
// The Good Stuff
// ============================== //

$(document).ready(function () {

    // Load the preset template.
    $.ajax('./templates/preset.jst', {
        success: function (data) {
            PRESET_HTML_TEMPLATE = data;
        }
    });

    // Make the dice form, the presets, and the log sortable. Added a bit
    // of delay to keep users from accidentally mucking up their layout while
    // swiping up and down on mobile devices.
    $('#middle-wrapper > .container-fluid > .row').sortable({
        handle: '.col-header',
        delay: 500,
        update: function (e, ui) {
            update_ui_cookie();
        }
    });

    // Create the listeners for preset actions.
    create_preset_actions_listeners();

    // ============================== //
    // Login Form
    // ============================== //

    /**
     * Triggered when the user submits the login form.
     */
    $('#login-form').submit(function (e) {

        var request = $('input[name="request"]', $(this)).val();
        var name = $('#login-form input[name="name"]').val();
        var password = $('#login-form input[name="password"]').val();

        if (name == '' || password == '') {
            alert('Please enter a profile name and password to ' + (request == 'save' ? 'save this' : 'load a') + ' profile.');
            e.preventDefault();
            return false;
        }

        if (request == 'save') {
            if (confirm('Are you sure you want to save this profile?') == false) {
                e.preventDefault();
                return false;
            } else {

                var presets = [];

                $('.preset-line').each(function () {
                    var preset_json = $(this).attr('data-preset').htmlEntityDecode();
                    var preset = JSON.parse(preset_json);
                    presets.push(preset);
                });

                var presets_json = JSON.stringify(presets).htmlEntityEncode();

                // Remove backslashes because it breaks the save process. Not
                // sure why, though. :(
                presets_json = presets_json.replace(/\\/g, '');

                // Pass the presets and log so they can be saved.
                $('#login-form input[name="presets"]').val(presets_json);
                $('#login-form textarea[name="log"]').val($('#log').val());

                // Update the UI cookie.
                update_ui_cookie();

            }
        } else if (request == 'load' && $('body').hasClass('existing')) {
            if (confirm('Are you sure you want to reload this profile?') == false) {
                e.preventDefault();
                return false;
            }
        }

    });

    /**
     * Triggered when the user clicks the 'New Profile' button.
     */
    $('#login-form button.new').click(function (e) {
        if (confirm('Are you sure you start a new profile?')) {
            window.location.href = './?request=new-profile';
        }
    });

    /**
     * Triggered when the user clicks the 'Load' button.
     */
    $('#login-form button.load').click(function (e) {
        $('#login-form input[name="request"]').val('load');
    });

    // ============================== //
    // Form
    // ============================== //

    /**
     * Triggered when the user presses a key. Prevents the preset form from
     * being submitted when the enter key is pressed.
     */
    $('#preset-input-wrapper input[name="name"]').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    /**
     * Triggered when the user places focus in a dice or preset input field.
     * This makes it easier for users to change dice values quickly.
     */
    $('#form-wrapper input:not([type="radio"])').focus(function (e) {
        $(this).select();
    });

    /**
     * Triggered when the user clicks the 'Reset' button on the preset form.
     * Clears all dice and preset form values.
     */
    $('#form-actions-wrapper .reset').click(function (e) {
        reset_dice_form();
    });

    /**
     * Triggered when the user clicks the 'Save Preset' button on the preset
     * form. Saves the existing preset.
     */
    $('#form-actions-wrapper .save-preset').click(function (e) {
        save_existing_preset();
    });

    /**
     * Triggered when the user clicks the 'Save New Preset' button on the
     * preset form. Saves a new preset using the form values.
     */
    $('#form-actions-wrapper .save-new-preset').click(function (e) {
        save_new_preset();
    });

    // ============================== //
    // Button Listeners & Alters
    // ============================== //

    /**
     * Triggered when the user enters text in the preset name or notes fields.
     * Enables or disables the 'Save New Preset' button.
     */
    $('input[name="name"], textarea[name="notes"]', $('#preset-input-wrapper')).keyup(function (e) {

        var name = $('input[name="name"]', $('#preset-input-wrapper')).val();
        var notes = $('textarea[name="notes"]', $('#preset-input-wrapper')).val();

        if (name != '' && notes != '') {
            // Enable the 'Save New Preset' button.
            $('.save-new-preset', $('#form-actions-wrapper')).prop('disabled', false);
        }

    });

    // ============================== //
    // Roll Buttons
    // ============================== //

    /**
     * Triggered when the user enters text in a dice form field. Prevents the
     * preset form from being submitted when the enter key is pressed. If the
     * user does press enter, simulate clicking the 'Roll' button for the
     * current die type instead.
     */
    $('#dice-input-wrapper input:not([type="radio"])').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $('button.roll', $(this).closest('.form-line')).click();
            e.preventDefault();
            return false;
        }
    });

    /**
     * Triggered when the user clicks the 'Roll' button. Rolls the specified
     * number of dice for the current die type.
     */
    $('.form-line button.roll').click(function (e) {

        var line = $(this).closest('.form-line');
        var type = line.attr('data-type');
        var count = parseInt($('input[name="count"]', line).val());
        var add = $('input[name="' + type + '-add"]:checked', line).val();
        var modifier = parseInt($('input[name="modifier"]', line).val());
        var max = line.attr('data-max');

        if (count == 0) {
            // User didn't specify the number of dice to roll.
            $('input[name="count"]', line).addClass('error').focus();
            alert('Please enter a number of dice to roll and try again.');
            e.preventDefault();
            return false;
        }

        if (type == 'dX') {
            // User selected a custom-sided die to roll.
            max = parseInt($('input[name="sides"]', line).val());
            type = 'd' + max.toString();
        }

        // Build the dice object.
        var dice = [
            {
                type: type,
                count: count,
                add: add,
                modifier: modifier,
                max: max
            }
        ];

        // Roll the dice!
        var roll = do_roll(dice);
        $('input[name="result"]', line).val(roll);

    });

    // ============================== //
    // Log
    // ============================== //

    /**
     * Triggered when the user clicks the 'Reset Log' button. Resets the log
     * to its original values.
     */
    $('#log-actions-wrapper .reset').click(function (e) {
        clear_log();
    });

});

/**
 * Resets the dice form. Typically called after saving or editing a preset.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function reset_dice_form() {
    console.log('Resetting dice form.');
    $('form[name="dice-form"]')[0].reset();
    $('form[name="dice-form"] input:not([type="radio"])').removeClass('error');
    $('.save-preset, .save-new-preset', $('#form-actions-wrapper')).prop('disabled', true);
    $('body').attr('data-current-preset-id', '');
}

/**
 * Generates a random number between the minimum and maximum values.
 *
 * @param min
 *   The minimum value.
 * @param max
 *   The maximum value.
 *
 * @returns int
 *   The generated random number.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function generate_random_number(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

/**
 * Performs a roll of the specified die or dice.
 *
 * @param dice
 *   An array of dice objects.
 *
 * @param name
 *   The name of the roll.
 *
 * @returns int
 *   The calculated total of the die roll or dice rolls.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function do_roll(dice, name) {

    var log = '';

    if (typeof(name) == 'undefined') {
        // Name not passed, use 'die' or 'dice' as the roll name.
        log += 'Rolling ' + (dice.length == 1 && dice[0].count == 1 ? 'die' : 'dice') + '...\n';
    } else {
        // Name passed, use it as the roll name.
        log += 'Rolling "' + name + '"...\n';
    }

    // Hold the calculated total.
    var total = 0;

    for (var i = 0; i < dice.length; i++) {

        var die = dice[i];
        var roll_total = 0;

        for (var j = 1; j <= die.count; j++) {

            // Roll this die.
            var roll = generate_random_number(1, die.max);

            if (j > 1) {
                log += ' + ';
            }

            // Update the log and current roll total.
            log += roll + ' (1' + die.type + ')';
            roll_total += roll;

        }

        if (die.modifier > 0) {
            // There is a modifier to add or subtract.
            if (die.add == 'add') {
                // Add the modifier.
                roll_total += die.modifier;
                log += ' + ' + die.modifier + ' (M)';
            } else if (die.add == 'subtract') {
                // Subtract the modifier.
                roll_total -= die.modifier;
                log += ' - ' + die.modifier + ' (M)';
            }
        }

        // Update the log and the calculated total.
        log += ' = ' + roll_total + '\n';
        total += roll_total;

    }

    // Update the log with the calculated total.
    log += '<strong>Total: ' + total + '</strong>\n';

    // Append the log.
    append_log(log.replace(/<(?:.|\n)*?>/gm, ''));

    // On mobile, show an alert (it's easier than the log).
    if (is_breakpoint('xs')) {

        var width = 400;

        if (is_breakpoint('xs')) {
            width = 280;
        }

        $('#dialog').html(log.replaceAll("\n", "<br />"));

        $('#dialog').dialog({
            width: width,
            title: 'Roll Result',
            draggable: false,
            modal: true,
            resizable: false,
            open: function () {
                $('.ui-widget-overlay').addClass('custom-overlay');
                $('body').addClass('noscroll');
            },
            close: function () {
                $('.ui-widget-overlay').removeClass('custom-overlay');
                $('body').removeClass('noscroll');
            }
        });

        $('.ui-dialog').position({
            my: "center",
            at: "center",
            of: $('.ui-widget-overlay')
        });

    }

    return total;

}

// ============================== //
// Log
// ============================== //

/**
 * Appends the specified message to the log.
 *
 * @param value
 *   The value to be appended.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function append_log(value) {
    $('#log').val(value + '\n' + $('#log').val());
    $('#log').scrollTop(0);
}

/**
 * Clears the log.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function clear_log() {
    $('#log').val('Log has been cleared.');
}

// ============================== //
// Presets
// ============================== //

/**
 * Builds an array of dice objects from the dice form.
 *
 * @returns array
 *   An array of dice objects.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function build_dice_array_from_form() {

    // Hold the dice objects.
    var dice = [];

    $('#form-wrapper .form-line').each(function () {

        var line = $(this);
        var type = line.attr('data-type');
        var count = parseInt($('input[name="count"]', line).val());
        var add = $('input[name="' + type + '-add"]:checked', line).val();
        var modifier = parseInt($('input[name="modifier"]', line).val());
        var max = line.attr('data-max');

        if (type == 'dX') {
            // This is a custom-sided die to roll.
            max = parseInt($('input[name="sides"]', line).val());
            type = 'd' + max.toString();
        }

        if (count > 0) {
            // Add this dice object to the dice array.
            dice.push({
                type: type,
                count: count,
                add: add,
                modifier: modifier,
                max: max
            });
        }

    });

    // Reverse the dice array so larger dice come first.
    dice.reverse();

    return dice;

}

/**
 * Builds the HTML for a preset by loading the preset markup and replacing
 * the placeholder values.
 *
 * @param preset
 *   The preset markup.
 *
 * @returns string
 *   The preset markup with replaced values.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function build_preset_html(preset) {

    var preset_json = JSON.stringify(preset).htmlEntityEncode();

    var preset_html = PRESET_HTML_TEMPLATE;
    preset_html = preset_html.replaceAll('!!_PRESET_ID_!!', generate_random_number(1, 999999));
    preset_html = preset_html.replaceAll('!!_PRESET_JSON_!!', preset_json);
    preset_html = preset_html.replaceAll('!!_PRESET_NAME_!!', preset.name);
    preset_html = preset_html.replaceAll('!!_PRESET_DICE_!!', format_dice_as_string(preset.dice));
    preset_html = preset_html.replaceAll('!!_PRESET_NOTE_!!', preset.notes);

    return preset_html;

}

/**
 * Creates the action listeners for preset actions (roll, edit, delete). First,
 * unbind any existing action listeners, then create new ones. This typically
 * happens whenever a new preset is created.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function create_preset_actions_listeners() {

    // Make the presets sortable.
    $('#presets-injection-target').sortable({
        handle: '.handle',
        helper: function (event, ui) {
            var clone = $(ui).clone();
            clone.css('position', 'absolute');
            return clone.get(0);
        }
    });

    // Unbind any previous click event listeners.
    $('.roll, .edit, .delete', $('.preset-line')).unbind('click');

    $('.preset-line .roll').click(function (e) {
        var preset_json = $(this).closest('.preset-line').attr('data-preset').htmlEntityDecode();
        var preset = JSON.parse(preset_json);
        do_roll(preset.dice, preset.name);
    });

    $('.preset-line .edit').click(function (e) {
        var preset_json = $(this).closest('.preset-line').attr('data-preset').htmlEntityDecode();
        var preset = JSON.parse(preset_json);
        edit_preset(preset, $(this));
    });

    $('.preset-line .delete').click(function (e) {
        if (confirm('Are you sure you want to delete this preset?')) {
            $(this).closest('.preset-line').animate({
                opacity: 0,
                height: 0,
                margin: 0,
                marginLeft: -15
            }, 250, function () {
                $(this).remove();
            });
        }
    });

}

/**
 * Formats the specified dice array as a string.
 *
 * @param dice
 *   The dice array.
 *
 * @returns string
 *   The formatted dice array in human-readable format.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function format_dice_as_string(dice) {

    output = '';
    i = 0;

    for (var i = 0; i < dice.length; i++) {

        var die = dice[i];

        output += i > 0 ? ' + ' : '';
        output += die.count + die.type;

        if (die.modifier > 0) {
            if (die.add == 'add') {
                output += ' + ' + die.modifier;
            } else if (die.add == 'subtract') {
                output += ' - ' + die.modifier;
            }
        }
    }

    return output;

}

/**
 * Populates the preset form with the specified preset values.
 *
 * @param data
 *   The preset JSON object.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function edit_preset(data, element) {

    // Force a form reset.
    $('#form-actions-wrapper .reset').click();

    // Store the current preset id for later.
    $('body').attr('data-current-preset-id', element.closest('.preset-line').attr('id'));

    // Set the preset name.
    var name = data.name;
    $('#preset-input-wrapper input[name="name"]').val(name);

    // Set the preset notes.
    var notes = data.notes;
    $('#preset-input-wrapper textarea[name="notes"]').val(notes);

    for (var i = 0; i < data.dice.length; i++) {

        // Get the die for the current dice object.
        var die = data.dice[i];

        // Set the die form values.
        var line = $('#dice-input-wrapper .form-line[data-type="' + die.type + '"]');
        $('input[name="count"]', line).val(die.count);
        $('input[name="modifier"]', line).val(die.modifier);
        $('input[name="' + die.type + '-add"][value="' + die.add + '"]').attr('checked', 'checked');

    }

    // Enable the 'Save Preset' and 'Save New Preset' buttons.
    $('.save-preset, .save-new-preset', $('#form-actions-wrapper')).prop('disabled', false);

}

/**
 * Saves an existing preset.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function save_existing_preset() {

    console.log('Saving existing preset.');

    var name = $('#preset-input-wrapper input[name="name"]').val();
    var notes = $('#preset-input-wrapper textarea[name="notes"]').val();
    var dice = build_dice_array_from_form();

    var preset = {
        name: name,
        notes: notes,
        dice: dice
    };

    // Build the new preset html.
    var preset_html = build_preset_html(preset);

    // Remove backslashes because it breaks the save process. Not sure why,
    // though. :(
    preset_html = preset_html.replace(/\\/g, '');

    // Get the current preset id.
    var current_preset_id = $('body').attr('data-current-preset-id');

    // Rename the old preset so we can insert our new preset after it, then
    // delete the old preset.
    var new_old_preset_id = current_preset_id + '-old';
    $('#' + current_preset_id).attr('id', new_old_preset_id);

    // Get the new... old... preset. :)
    var new_old_preset = $('#' + new_old_preset_id);

    // Insert the new preset and remove the old preset.
    $(preset_html).insertAfter(new_old_preset);
    new_old_preset.remove();

    // Reset the dice form and (re)create the preset action listeners.
    reset_dice_form();
    create_preset_actions_listeners();

}

/**
 * Saves a new preset.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function save_new_preset() {

    var name = $('#preset-input-wrapper input[name="name"]').val();
    var notes = $('#preset-input-wrapper textarea[name="notes"]').val();
    var dice = build_dice_array_from_form();

    var preset = {
        name: name,
        notes: notes,
        dice: dice
    };

    // Build the new preset html.
    var preset_html = build_preset_html(preset);

    // Remove backslashes because it breaks the save process. Not sure why,
    // though. :(
    preset_html = preset_html.replace(/\\/g, '');

    // Prepend the new preset to the presets injection target.
    $('#presets-injection-target').prepend(preset_html);

    // Reset the dice form and (re)create the preset action listeners.
    reset_dice_form();
    create_preset_actions_listeners();

}

/**
 * Updates the UI cookie to provide a seamless experience between sessions.
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function update_ui_cookie() {

    var name = $('#login-form input[name="name"]').val();
    var sortables = $('#middle-wrapper > .container-fluid > .row').sortable('toArray');

    var values = {
        name: name,
        sortables: sortables
    };

    var json = JSON.stringify(values);

    // Set the cookie for this path, for one year.
    Cookies.set('ui', json, {expires: 365, path: './'});

    // Update the hidden form element.
    $('#login-form textarea[name="ui"]').val(json);

}

/**
 * Determines if the specified Bootstrap breakpoint is visible.
 *
 * @param alias
 *
 * @returns bool
 *
 * @author Jimmy K. <jimmy@ghazlawl.com>
 */
function is_breakpoint(alias) {
    return $('.device-' + alias).is(':visible');
}
