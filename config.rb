require 'compass/import-once/activate'

css_dir = "resources/css"
sass_dir = "resources/scss"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
