<div id="presets-sortable-col" class="col col-xs-12 col-sm-4">
  <div id="presets-wrapper" class="col-inner">
    <div class="col-header">
      <div class="row">
        <div class="col col-xs-12 presets">
          Presets
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.col-header -->
    <div class="col-content">
      <div id="presets-injection-target">
        <?php if ($existing_presets) : ?>
          <?php $preset_template = file_get_contents('./templates/preset.jst'); ?>
          <?php foreach ($existing_presets as $k => $v) : ?>
            <?php

            $preset_html = $preset_template;
            $preset_html = str_replace('!!_PRESET_ID_!!', rand(1, 999999), $preset_html);
            $preset_html = str_replace('!!_PRESET_JSON_!!', str_replace('"', '&quot;', json_encode($v)), $preset_html);
            $preset_html = str_replace('!!_PRESET_NAME_!!', $v->name, $preset_html);
            $preset_html = str_replace('!!_PRESET_DICE_!!', format_dice_as_string($v->dice), $preset_html);
            $preset_html = str_replace('!!_PRESET_NOTE_!!', $v->notes, $preset_html);
            echo $preset_html;

            ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div><!-- /#presets-injection-target -->
    </div><!-- /.col-content -->
  </div><!-- /#presets-wrapper -->
</div><!-- /.col -->
