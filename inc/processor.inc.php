<?php

session_start();

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache");

if ($_GET['request'] == 'new-profile') {
  unset($_SESSION['name']);
  unset($_SESSION['password']);
  unset($_SESSION['filename']);
  header('location: ./');
  exit;
}

if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {

  if ($_POST['request'] == 'save' || $_POST['request'] == 'load') {

    // Get the POST values.
    $name = stripslashes($_POST['name']);
    $password = stripslashes($_POST['password']);
    $log = stripslashes($_POST['log']);

    // Get the presets.
    $presets = str_replace('&quot;', '"', stripslashes($_POST['presets']));
    $presets = json_decode($presets);

    // Get the UI.
    $ui = str_replace('&quot;', '"', stripslashes($_POST['ui']));
    $ui = json_decode($ui);

    // Generate the md5 name/password using the posted name/password.
    $md5_name = md5(strtolower($name));
    $md5_password = md5(strtolower($password));

    // Generate the filename.
    $filename = $md5_name . '.json';

  }

  if ($_POST['request'] == 'save') {

    // Construct the data.
    $data = [
      'name' => $name,
      'password' => $md5_password,
      'presets' => $presets,
      'log' => $log,
      'ui' => $ui,
    ];

    // Encode the data for writing.
    $json = json_encode($data);

    // If the file exists...
    if (file_exists('./files/' . $filename)) {

      $contents = file_get_contents('./files/' . $filename);
      $existing_json = json_decode($contents);

      if ($existing_json->password == $md5_password) {

        // Save the filename in the session so we don't have to authenticate each time.
        $_SESSION['name'] = $name;
        $_SESSION['password'] = $password;
        $_SESSION['filename'] = $filename;

        // Passwords match!
        if ($file = fopen('./files/' . $filename, 'w+') or die("Unable to open file!")) {

          // Save the data.
          fwrite($file, $json);
          fclose($file);

          $_SESSION['message'] = [
            'message' => 'Profile saved!',
            'class' => 'success',
          ];

        }

        // Load the updated JSON.
        $contents = file_get_contents('./files/' . $filename);
        $existing_json = json_decode($contents);

      } else {

        $_SESSION['message'] = [
          'message' => 'Passsword is incorrect.',
          'class' => 'error',
        ];

      }

    } elseif ($file = fopen('./files/' . $filename, 'w+') or die("Unable to open file!")) {

      // File doesn't exist; create it.
      fwrite($file, $json);
      fclose($file);

      $_SESSION['message'] = [
        'message' => 'New profile created!',
        'class' => 'success',
      ];

    }

  } elseif ($_POST['request'] == 'load') {

    // If the file exists...
    if (file_exists('./files/' . $filename)) {

      $contents = file_get_contents('./files/' . $filename);
      $existing_json = json_decode($contents);

      if ($existing_json->password == $md5_password) {

        // Save the filename in the session so we don't have to authenticate each time.
        $_SESSION['name'] = $name;
        $_SESSION['password'] = $password;
        $_SESSION['filename'] = $filename;

        $_SESSION['message'] = [
          'message' => 'Profile loaded!',
          'class' => 'success',
        ];

      } else {

        $_SESSION['message'] = [
          'message' => 'Password is incorrect.',
          'class' => 'error',
        ];

      }

    }

  }

  header('location: ./');
  exit;

}

if (isset($_SESSION['filename'])) {

  $contents = file_get_contents('./files/' . $_SESSION['filename']);
  $existing_json = json_decode($contents);

}

// ============================== //
// Die Types
// ============================== //

$die_types = [
  'd4' => [
    'max' => 4,
  ],
  'd6' => [
    'max' => 6,
  ],
  'd8' => [
    'max' => 8,
  ],
  'd10' => [
    'max' => 10,
  ],
  'd12' => [
    'max' => 12,
  ],
  'd20' => [
    'max' => 20,
  ],
  'd30' => [
    'max' => 30,
  ],
  'd100' => [
    'max' => 100,
  ],
];

// ============================== //
// Presets - These will be
// deleted once the presets are
// actually saving and loading.
// ============================== //

/*
$existing_presets = [
  [
    'name' => 'Ranged Basic Attack (ATK)',
    'notes' => '+3 from Dexterity, +3 from Proficiency, +2 from Fighting Style (Archery).',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 8,
      ],
    ],
  ],
  [
    'name' => 'Ranged Basic Attack w/ Colossus Slayer (DMG)',
    'notes' => 'Only applies if target is below hit point maximum and Hunter\'s Mark is not applied. 1d8 from attack, 1d8 from Colossus Slayer, +3 from Dexterity, +2 from Bracers, +3 from Proficiency, +1 from Weapon.',
    'dice' => [
      [
        'type' => 'd8',
        'max' => 8,
        'count' => 2,
        'add' => 'add',
        'modifier' => 8,
      ],
    ],
  ],
  [
    'name' => 'Ranged Basic Attack w/ Hunter\'s Mark (DMG)',
    'notes' => 'Only applies if Hunter\'s Mark is applied and target is at full health. 1d8 from attack, 1d6 from Hunter\'s Mark, +3 from Dexterity, +2 from Bracers, +3 from Proficiency, +1 from Weapon.',
    'dice' => [
      [
        'type' => 'd8',
        'max' => 8,
        'count' => 1,
        'add' => 'add',
        'modifier' => 8,
      ],
      [
        'type' => 'd6',
        'max' => 6,
        'count' => 1,
        'add' => 'add',
        'modifier' => 0,
      ],
    ],
  ],
  [
    'name' => 'Ranged Basic Attack w/ Colossus Slayer & Hunter\'s Mark (DMG)',
    'notes' => 'Only applies if target is below hit point maximum and Hunter\'s Mark is applied. 1d8 from attack, 1d8 from Colossus Slayer, 1d6 from Hunter\'s Mark, +3 from Dexterity, +2 from Bracers, +3 from Proficiency, +1 from Weapon.',
    'dice' => [
      [
        'type' => 'd8',
        'max' => 8,
        'count' => 2,
        'add' => 'add',
        'modifier' => 8,
      ],
      [
        'type' => 'd6',
        'max' => 6,
        'count' => 1,
        'add' => 'add',
        'modifier' => 0,
      ],
    ],
  ],
  [
    'name' => 'Ranged Basic Attack (DMG)',
    'notes' => '1d8 from attack, 1d8 from Colossus Slayer, , +3 from Dexterity, +2 from Bracers, +3 from Proficiency, +1 from Weapon.',
    'dice' => [
      [
        'type' => 'd8',
        'max' => 8,
        'count' => 2,
        'add' => 'add',
        'modifier' => 8,
      ],
    ],
  ],
  [
    'name' => 'Melee Basic Attack (ATK)',
    'notes' => '+3 from Dexterity, +3 from Proficiency.',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 6,
      ],
    ],
  ],
  [
    'name' => 'Melee Basic Attack (DMG)',
    'notes' => '',
    'dice' => [
      [
        'type' => 'd6',
        'max' => 6,
        'count' => 1,
        'add' => 'add',
        'modifier' => 4,
      ],
    ],
  ],
  [
    'name' => 'Gray Bag of Tricks',
    'notes' => 'Use an action to pull a small, fuzzy object out. Object can be thrown up to 20 feet. When it lands, it transforms into a creature based on a d8 roll.',
    'dice' => [
      [
        'type' => 'd8',
        'max' => 8,
        'count' => 1,
        'add' => 'add',
        'modifier' => 0,
      ],
    ],
  ],
  [
    'name' => 'Drunk Master (Home Brew)',
    'notes' => 'If you roll a 1 and are attacking, attack a random ally. While conscious, if you roll a 2-25, no effect; if you roll a 1-10, throw up and roll one square in a random direction. The previously-occupied square becomes difficult terrain until your next turn. If you roll a 25-27, fall down prone; use an action to stand up; if you roll a 28-29, go unconscious. Need to roll a saving throw to wake up. If you roll a 30, stumble and dodge the current attack.',
    'dice' => [
      [
        'type' => 'd30',
        'max' => 30,
        'count' => 1,
        'add' => 'add',
        'modifier' => 0,
      ],
    ],
  ],
  [
    'name' => 'Initiative',
    'notes' => '+3 for Initiative Bonus.',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 3,
      ],
    ],
  ],
  [
    'name' => 'Perception',
    'notes' => '+5 for Perception Skill.',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 5,
      ],
    ],
  ],
];
*/

$existing_presets = [
  (object) [
    'name' => 'Ranged Basic Attack (ATK)',
    'notes' => '+3 from Dexterity, +3 from Proficiency, +2 from Fighting Style (Archery).',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 8,
      ],
    ],
  ],
  (object) [
    'name' => 'Melee Basic Attack (ATK)',
    'notes' => '+3 from Dexterity, +3 from Proficiency.',
    'dice' => [
      [
        'type' => 'd20',
        'max' => 20,
        'count' => 1,
        'add' => 'add',
        'modifier' => 6,
      ],
    ],
  ],
];

if ($existing_json) {
  $existing_presets = $existing_json->presets;
}
