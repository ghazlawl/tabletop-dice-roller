<?php

/**
 * Format a dice array as string.
 *
 * @param $dice
 * @return string
 */
function format_dice_as_string($dice) {

  if (!$dice) {
    return '';
  }

  $output = '';
  $i = 0;

  foreach ($dice as $die) {

    $output .= $i++ > 0 ? ' + ' : '';
    $output .= $die->count . $die->type;

    if ($die->modifier > 0) {
      if ($die->add == 'add') {
        $output .= ' + ' . $die->modifier;
      } elseif ($die->add == 'subtract') {
        $output .= ' - ' . $die->modifier;
      }
    }
  }

  return $output;

}
