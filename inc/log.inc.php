<div id="log-sortable-col" class="col col-xs-12 col-sm-4">
  <div id="log-wrapper" class="col-inner">
    <div class="col-header">
      <div class="row">
        <div class="col col-xs-12 log">
          Log
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.col-header -->
    <div class="col-content">
      <div id="log-textarea-wrapper">
        <textarea name="log" id="log"><?php echo isset($existing_json) && !empty($existing_json->log) ? $existing_json->log : 'This field records your latest rolls.'; ?></textarea>
      </div><!-- /#log-textarea-wrapper -->
      <div id="log-actions-wrapper" class="actions-wrapper">
        <button class="reset">Clear Log</button>
      </div><!-- /#log-actions-wrapper -->
    </div><!-- /.col-content -->
  </div><!-- /#log-wrapper -->
</div><!-- /.col -->
