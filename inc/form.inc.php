<div id="form-sortable-col" class="col col-xs-12 col-sm-4">
  <div id="form-wrapper" class="col-inner">
    <div class="col-header">
      <div class="row">
        <div class="col col-xs-2 die">
          Die
        </div><!-- /.col -->
        <div class="col col-xs-2 count">
          #
        </div><!-- /.col -->
        <div class="col col-xs-2 add">
          + / -
        </div><!-- /.col -->
        <div class="col col-xs-2 modifier">
          Modifier
        </div><!-- /.col -->
        <div class="col col-xs-2 roll">
          &nbsp;
        </div><!-- /.col -->
        <div class="col col-xs-2 result">
          Result
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.col-header -->
    <div class="col-content">
      <form name="dice-form" onsubmit="return false;">
        <div id="dice-input-wrapper">
          <?php foreach ($die_types as $k => $v) : ?>
            <div class="form-line" data-type="<?php echo $k; ?>" data-max="<?php echo $v['max']; ?>">
              <div class="row">
                <div class="col col-xs-2 die">
                  <div class="sprite dice <?php echo $k; ?>" title="<?php echo $k; ?>"><?php echo $k; ?></div>
                </div><!-- /.col -->
                <div class="col col-xs-2 count">
                  <input type="number" name="count" value="0"/>
                </div><!-- /.col -->
                <div class="col col-xs-2 add">
                  <input type="radio" name="<?php echo $k; ?>-add" value="add" tabindex="-1" checked/>
                  <input type="radio" name="<?php echo $k; ?>-add" value="subtract" tabindex="-1"/>
                </div><!-- /.col -->
                <div class="col col-xs-2 modifier">
                  <input type="number" name="modifier" value="0"/>
                </div><!-- /.col -->
                <div class="col col-xs-2 roll">
                  <button name="roll" class="roll">Roll</button>
                </div><!-- /.col -->
                <div class="col col-xs-2 result">
                  <input type="number" name="result" value="0" disabled/>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.form-line -->
          <?php endforeach; ?>
          <div class="form-line" data-type="dX">
            <div class="row">
              <div class="col col-xs-2 die">
                <input type="number" name="sides" value="50"/>
              </div><!-- /.col -->
              <div class="col col-xs-2 count">
                <input type="number" name="count" value="0"/>
              </div><!-- /.col -->
              <div class="col col-xs-2 add">
                <input type="radio" name="dX-add" value="add" tabindex="-1" checked/>
                <input type="radio" name="dX-add" value="subtract" tabindex="-1"/>
              </div><!-- /.col -->
              <div class="col col-xs-2 modifier">
                <input type="number" name="modifier" value="0"/>
              </div><!-- /.col -->
              <div class="col col-xs-2 roll">
                <button name="roll" class="roll">Roll</button>
              </div><!-- /.col -->
              <div class="col col-xs-2 result">
                <input type="number" name="result" value="0" disabled/>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.form-line -->
        </div><!-- /#dice-input-wrapper -->
        <div id="preset-input-wrapper">
          <div class="form-line">
            <div class="row">
              <div class="col col-xs-12">
                <label for="name">Preset Name</label>
                <input type="text" name="name" placeholder="Ranged Basic Attack" autocomplete="off"/>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.form-line -->
          <div class="form-line">
            <div class="row">
              <div class="col col-xs-12">
                <label for="notes">Preset Notes</label>
                <textarea name="notes" placeholder="Notes go here..."></textarea>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.form-line -->
        </div><!-- /#preset-input-wrapper -->
        <div id="form-actions-wrapper" class="actions-wrapper">
          <button class="save-preset" disabled>Save Preset</button>
          <span class="or">or</span>
          <button class="save-new-preset" disabled>Save New Preset</button>
          <button class="reset">Reset</button>
        </div><!-- /#form-actions-wrapper -->
      </form>
    </div><!-- /#form-wrapper -->
  </div><!-- /.col-content -->
</div><!-- /.col -->
